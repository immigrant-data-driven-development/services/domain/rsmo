# 📕Documentation: Measurement



## 🌀 Package's Data Model

![Domain Diagram](classdiagram.png)

### ⚡Entities

* **Measure** : Function used to associate a value (Measured Value) to a Measurable Element of a Measurable Entity. Measures quantify Measurable Elements and characterize Measurable Entity Types. 
* **MeasurableElement** : Measurable property that characterizes a Measurable Entity Type (e.g., height, Code Quality).
* **ProjectUnderMeasurement** : -
* **Measurement_Measure_MeasurableEntity** : Entity with data about Measurement, Measure and Measureentity
* **Measurement** : Action that measures a Measurable Element of a Measurable Entity by applying a Measure and adopting a Measurement Procedure to obtain a Measured Value. A Measurement is performed based on a Measurement Planning Item. 
* **MeasurableEntity** : Anything that can be measured (e.g. a code)
