package br.nemo.immigrant.ontology.service.rsmo.measurement.repositories;

import br.nemo.immigrant.ontology.entity.rsmo.measurement.models.Measurement;
import br.nemo.immigrant.ontology.entity.rsmo.measurement.repositories.MeasurementRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "measurement", path = "measurement")
public interface MeasurementRepositoryWeb extends MeasurementRepository {

}
