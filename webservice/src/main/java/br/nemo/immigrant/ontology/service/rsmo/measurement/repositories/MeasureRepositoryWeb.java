package br.nemo.immigrant.ontology.service.rsmo.measurement.repositories;

import br.nemo.immigrant.ontology.entity.rsmo.measurement.models.Measure;
import br.nemo.immigrant.ontology.entity.rsmo.measurement.repositories.MeasureRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "measure", path = "measure")
public interface MeasureRepositoryWeb extends MeasureRepository {

}
