package br.nemo.immigrant.ontology.service.rsmo.measurement.controllers;

import br.nemo.immigrant.ontology.entity.rsmo.measurement.models.MeasurableElement;
import br.nemo.immigrant.ontology.entity.rsmo.measurement.repositories.MeasurableElementRepository;
import br.nemo.immigrant.ontology.service.rsmo.measurement.records.MeasurableElementInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class MeasurableElementController  {

  @Autowired
  MeasurableElementRepository repository;

  @QueryMapping
  public List<MeasurableElement> findAllMeasurableElements() {
    return repository.findAll();
  }

  @QueryMapping
  public MeasurableElement findByIDMeasurableElement(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public MeasurableElement createMeasurableElement(@Argument MeasurableElementInput input) {
    MeasurableElement instance = MeasurableElement.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public MeasurableElement updateMeasurableElement(@Argument Long id, @Argument MeasurableElementInput input) {
    MeasurableElement instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("MeasurableElement not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteMeasurableElement(@Argument Long id) {
    repository.deleteById(id);
  }

}
