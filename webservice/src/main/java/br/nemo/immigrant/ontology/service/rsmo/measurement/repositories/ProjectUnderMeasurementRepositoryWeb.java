package br.nemo.immigrant.ontology.service.rsmo.measurement.repositories;

import br.nemo.immigrant.ontology.entity.rsmo.measurement.models.ProjectUnderMeasurement;
import br.nemo.immigrant.ontology.entity.rsmo.measurement.repositories.ProjectUnderMeasurementRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "projectundermeasurement", path = "projectundermeasurement")
public interface ProjectUnderMeasurementRepositoryWeb extends ProjectUnderMeasurementRepository {

}
