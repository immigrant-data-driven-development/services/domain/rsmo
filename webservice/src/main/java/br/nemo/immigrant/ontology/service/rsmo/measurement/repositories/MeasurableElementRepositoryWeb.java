package br.nemo.immigrant.ontology.service.rsmo.measurement.repositories;

import br.nemo.immigrant.ontology.entity.rsmo.measurement.models.MeasurableElement;
import br.nemo.immigrant.ontology.entity.rsmo.measurement.repositories.MeasurableElementRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "measurableelement", path = "measurableelement")
public interface MeasurableElementRepositoryWeb extends MeasurableElementRepository {

}
