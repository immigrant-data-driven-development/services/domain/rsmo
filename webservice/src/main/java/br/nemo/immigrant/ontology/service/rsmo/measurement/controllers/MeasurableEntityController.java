package br.nemo.immigrant.ontology.service.rsmo.measurement.controllers;

import br.nemo.immigrant.ontology.entity.rsmo.measurement.models.MeasurableEntity;
import br.nemo.immigrant.ontology.entity.rsmo.measurement.repositories.MeasurableEntityRepository;
import br.nemo.immigrant.ontology.service.rsmo.measurement.records.MeasurableEntityInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class MeasurableEntityController  {

  @Autowired
  MeasurableEntityRepository repository;

  @QueryMapping
  public List<MeasurableEntity> findAllMeasurableEntitys() {
    return repository.findAll();
  }

  @QueryMapping
  public MeasurableEntity findByIDMeasurableEntity(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public MeasurableEntity createMeasurableEntity(@Argument MeasurableEntityInput input) {
    MeasurableEntity instance = MeasurableEntity.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public MeasurableEntity updateMeasurableEntity(@Argument Long id, @Argument MeasurableEntityInput input) {
    MeasurableEntity instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("MeasurableEntity not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteMeasurableEntity(@Argument Long id) {
    repository.deleteById(id);
  }

}
