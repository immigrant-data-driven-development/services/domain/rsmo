package br.nemo.immigrant.ontology.service.rsmo.measurement.repositories;

import br.nemo.immigrant.ontology.entity.rsmo.measurement.models.MeasurementMeasureMeasurableEntity;
import br.nemo.immigrant.ontology.entity.rsmo.measurement.repositories.MeasurementMeasureMeasurableEntityRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "measurement_measure_measurableentity", path = "measurement_measure_measurableentity")
public interface MeasurementMeasureMeasurableEntityRepositoryWeb extends MeasurementMeasureMeasurableEntityRepository {

}
