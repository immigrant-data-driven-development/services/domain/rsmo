package br.nemo.immigrant.ontology.service.rsmo.measurement.repositories;

import br.nemo.immigrant.ontology.entity.rsmo.measurement.models.MeasurableEntity;
import br.nemo.immigrant.ontology.entity.rsmo.measurement.repositories.MeasurableEntityRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "measurableentity", path = "measurableentity")
public interface MeasurableEntityRepositoryWeb extends MeasurableEntityRepository {

}
