package br.nemo.immigrant.ontology.service.rsmo.measurement.controllers;

import br.nemo.immigrant.ontology.entity.rsmo.measurement.models.MeasurementMeasureMeasurableEntity;
import br.nemo.immigrant.ontology.entity.rsmo.measurement.repositories.MeasurementMeasureMeasurableEntityRepository;
import br.nemo.immigrant.ontology.service.rsmo.measurement.records.MeasurementMeasureMeasurableEntityInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class MeasurementMeasureMeasurableEntityController  {

  @Autowired
  MeasurementMeasureMeasurableEntityRepository repository;

  @QueryMapping
  public List<MeasurementMeasureMeasurableEntity> findAllMeasurementMeasureMeasurableEntitys() {
    return repository.findAll();
  }

  @QueryMapping
  public MeasurementMeasureMeasurableEntity findByIDMeasurementMeasureMeasurableEntity(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public MeasurementMeasureMeasurableEntity createMeasurementMeasureMeasurableEntity(@Argument MeasurementMeasureMeasurableEntityInput input) {
    MeasurementMeasureMeasurableEntity instance = MeasurementMeasureMeasurableEntity.builder().measurableValue(input.measurableValue()).build();

    return repository.save(instance);
  }

  @MutationMapping
  public MeasurementMeasureMeasurableEntity updateMeasurementMeasureMeasurableEntity(@Argument Long id, @Argument MeasurementMeasureMeasurableEntityInput input) {
    MeasurementMeasureMeasurableEntity instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("MeasurementMeasureMeasurableEntity not found");
    }
    instance.setMeasurableValue(input.measurableValue());
    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteMeasurementMeasureMeasurableEntity(@Argument Long id) {
    repository.deleteById(id);
  }

}
