package br.nemo.immigrant.ontology.service.rsmo.measurement.controllers;

import br.nemo.immigrant.ontology.entity.rsmo.measurement.models.ProjectUnderMeasurement;
import br.nemo.immigrant.ontology.entity.rsmo.measurement.repositories.ProjectUnderMeasurementRepository;
import br.nemo.immigrant.ontology.service.rsmo.measurement.records.ProjectUnderMeasurementInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ProjectUnderMeasurementController  {

  @Autowired
  ProjectUnderMeasurementRepository repository;

  @QueryMapping
  public List<ProjectUnderMeasurement> findAllProjectUnderMeasurements() {
    return repository.findAll();
  }

  @QueryMapping
  public ProjectUnderMeasurement findByIDProjectUnderMeasurement(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public ProjectUnderMeasurement createProjectUnderMeasurement(@Argument ProjectUnderMeasurementInput input) {
    ProjectUnderMeasurement instance = ProjectUnderMeasurement.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public ProjectUnderMeasurement updateProjectUnderMeasurement(@Argument Long id, @Argument ProjectUnderMeasurementInput input) {
    ProjectUnderMeasurement instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("ProjectUnderMeasurement not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteProjectUnderMeasurement(@Argument Long id) {
    repository.deleteById(id);
  }

}
