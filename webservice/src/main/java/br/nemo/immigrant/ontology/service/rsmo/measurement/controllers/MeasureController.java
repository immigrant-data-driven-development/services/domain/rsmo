package br.nemo.immigrant.ontology.service.rsmo.measurement.controllers;

import br.nemo.immigrant.ontology.entity.rsmo.measurement.models.Measure;
import br.nemo.immigrant.ontology.entity.rsmo.measurement.repositories.MeasureRepository;
import br.nemo.immigrant.ontology.service.rsmo.measurement.records.MeasureInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class MeasureController  {

  @Autowired
  MeasureRepository repository;

  @QueryMapping
  public List<Measure> findAllMeasures() {
    return repository.findAll();
  }

  @QueryMapping
  public Measure findByIDMeasure(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public Measure createMeasure(@Argument MeasureInput input) {
    Measure instance = Measure.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public Measure updateMeasure(@Argument Long id, @Argument MeasureInput input) {
    Measure instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("Measure not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteMeasure(@Argument Long id) {
    repository.deleteById(id);
  }

}
