package br.nemo.immigrant.ontology.service.rsmo.measurement.controllers;

import br.nemo.immigrant.ontology.entity.rsmo.measurement.models.Measurement;
import br.nemo.immigrant.ontology.entity.rsmo.measurement.repositories.MeasurementRepository;
import br.nemo.immigrant.ontology.service.rsmo.measurement.records.MeasurementInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class MeasurementController  {

  @Autowired
  MeasurementRepository repository;

  @QueryMapping
  public List<Measurement> findAllMeasurements() {
    return repository.findAll();
  }

  @QueryMapping
  public Measurement findByIDMeasurement(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public Measurement createMeasurement(@Argument MeasurementInput input) {
    Measurement instance = Measurement.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public Measurement updateMeasurement(@Argument Long id, @Argument MeasurementInput input) {
    Measurement instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("Measurement not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteMeasurement(@Argument Long id) {
    repository.deleteById(id);
  }

}
