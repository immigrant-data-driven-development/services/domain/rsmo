ALTER TABLE public.measure  REPLICA IDENTITY FULL;
ALTER TABLE public.measurableelement  REPLICA IDENTITY FULL;
ALTER TABLE public.projectundermeasurement  REPLICA IDENTITY FULL;
ALTER TABLE public.measurement_measure_measurableentity  REPLICA IDENTITY FULL;
ALTER TABLE public.measurement  REPLICA IDENTITY FULL;
ALTER TABLE public.measurableentity  REPLICA IDENTITY FULL;
