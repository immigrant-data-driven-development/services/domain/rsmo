package br.nemo.immigrant.ontology.entity.rsmo.measurement.repositories;

import br.nemo.immigrant.ontology.entity.rsmo.measurement.models.MeasurableEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import org.springframework.data.jpa.repository.JpaRepository;
public interface MeasurableEntityRepository extends JpaRepository<MeasurableEntity, Long>, PagingAndSortingRepository<MeasurableEntity, Long>, ListCrudRepository<MeasurableEntity, Long> {

  Optional<IDProjection> findByInternalId(String internalId);

  Boolean existsByInternalId(String internalId);


}
