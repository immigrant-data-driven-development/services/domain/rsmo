package br.nemo.immigrant.ontology.entity.rsmo.measurement.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;




@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "measurement_measure_measurableentity")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class MeasurementMeasureMeasurableEntity  implements Serializable {


        @Id
        private @GeneratedValue (strategy=GenerationType.IDENTITY)
        Long id;


  private String measurableValue;

  @OneToOne
  @JoinColumn(name = "measurement_id", referencedColumnName = "id")
  private Measurement measurement;

  @OneToOne
  @JoinColumn(name = "measure_id", referencedColumnName = "id")
  private Measure measure;

  @OneToOne
  @JoinColumn(name = "measurableentity_id", referencedColumnName = "id")
  private MeasurableEntity measurableentity;

  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

          MeasurementMeasureMeasurableEntity elem = (MeasurementMeasureMeasurableEntity) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "Measurement_Measure_MeasurableEntity {" +
         "id="+this.id+
          ", measurableValue='"+this.measurableValue+"'"+

      '}';
  }
}
