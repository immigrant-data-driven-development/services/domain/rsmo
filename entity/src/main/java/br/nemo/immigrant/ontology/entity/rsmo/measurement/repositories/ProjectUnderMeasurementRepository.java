package br.nemo.immigrant.ontology.entity.rsmo.measurement.repositories;

import br.nemo.immigrant.ontology.entity.rsmo.measurement.models.ProjectUnderMeasurement;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import org.springframework.data.jpa.repository.JpaRepository;
public interface ProjectUnderMeasurementRepository extends JpaRepository<ProjectUnderMeasurement, Long>, PagingAndSortingRepository<ProjectUnderMeasurement, Long>, ListCrudRepository<ProjectUnderMeasurement, Long> {

  Optional<IDProjection> findByInternalId(String internalId);

  Boolean existsByInternalId(String internalId);


}
