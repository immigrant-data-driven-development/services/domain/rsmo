package br.nemo.immigrant.ontology.entity.rsmo.measurement.repositories;

import br.nemo.immigrant.ontology.entity.rsmo.measurement.models.MeasurementMeasureMeasurableEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import org.springframework.data.jpa.repository.JpaRepository;
public interface MeasurementMeasureMeasurableEntityRepository extends JpaRepository<MeasurementMeasureMeasurableEntity, Long>, PagingAndSortingRepository<MeasurementMeasureMeasurableEntity, Long>, ListCrudRepository<MeasurementMeasureMeasurableEntity, Long> {

  

}
