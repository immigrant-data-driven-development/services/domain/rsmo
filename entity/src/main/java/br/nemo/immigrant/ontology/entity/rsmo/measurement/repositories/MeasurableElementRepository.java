package br.nemo.immigrant.ontology.entity.rsmo.measurement.repositories;

import br.nemo.immigrant.ontology.entity.rsmo.measurement.models.MeasurableElement;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import org.springframework.data.jpa.repository.JpaRepository;
public interface MeasurableElementRepository extends JpaRepository<MeasurableElement, Long>, PagingAndSortingRepository<MeasurableElement, Long>, ListCrudRepository<MeasurableElement, Long> {

  Optional<IDProjection> findByInternalId(String internalId);

  Boolean existsByInternalId(String internalId);


}
