# rsmo
## 🚀 Goal
Reference Software Measurement Ontology (RSMO)

## 📕 Domain Documentation

Domain documentation can be found [here](./docs/README.md)

## ⚙️ Requirements

1. Postgresql
2. Java 17
3. Maven

## ⚙️ Stack
1. Micronaut 3.8.2


## 🔧 Install

1) Create a database with name rsmo with **CREATE DATABASE rsmo**.
2) Run the command to start the webservice and create table of database:

```bash
mvn Spring-boot:run
```

## Debezium

Go to folder named *register* and performs following command to register in debezium:

```bash
curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:8083/connectors/ -d @register-sro.json
```

To delete, uses:

```bash
curl -i -X DELETE localhost:8083/connectors/sro-connector/
```


## 🔧 Usage

* Access [http://localhost:](http://localhost:__«SKIP^NEW^LINE^IF^EMPTY»__) to see Swagger
* Acess [http://localhost:/grapiql](http://localhost:__«SKIP^NEW^LINE^IF^EMPTY»__/grapiql) to see Graphql.

## ✒️ Team

* **[](__«SKIP^NEW^LINE^IF^EMPTY»__)**

## 📕 Literature

